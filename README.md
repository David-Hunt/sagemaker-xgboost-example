# Sagemaker XGBoost Example
This repository is complimentary to [this blog post](https://fathomtech.io/blog/training-and-deploying-in-aws-sagemaker/)

The Jupyter Notebook is meant to be used within AWS SageMaker. AWS SageMaker is a fully managed service that provides  the ability to build, train, and deploy machine learning models quickly. 

The notebook contains steps for the preparation of data, the training of a model with XGBoost and the deploying of the model to a sagemaker endpoint. The notebook is adapted form the following [example](https://github.com/awslabs/amazon-sagemaker-examples/blob/master/introduction_to_applying_machine_learning/xgboost_direct_marketing/xgboost_direct_marketing_sagemaker.ipynb)

The data used in this project can be found [here](https://sagemaker-sample-data-us-west-2.s3-us-west-2.amazonaws.com/autopilot/direct_marketing/bank-additional.zip).
The data contains over 40,000 customer records, with 20 features per customer. The features are mixed; some numeric, some categorical. The data is used for targeting direct marketing effectively.

To find out more please refer to the following blog posts:  
* [Preparing Your Data For AWS Sagemaker](https://fathomtech.io/blog/preparing-your-data-for-aws-sagemaker/)  
* [Training and Deploying Machine Learning Models with AWS Sagemaker](https://fathomtech.io/blog/training-and-deploying-in-aws-sagemaker/)